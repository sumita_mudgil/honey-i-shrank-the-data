'use strict';

angular.module('myApp.view1', ['ngRoute'])

.config(['$routeProvider', function($routeProvider) {
  $routeProvider.when('/view1', {
    templateUrl: 'view1/view1.html',
    controller: 'View1Ctrl'
  });
}])
 
.controller('View1Ctrl', ['$scope','$http',function($scope,$http) {
        function init1(){
   $scope.request1={
    site_domain:'',
    start_date:'',
    end_date:'',
    post_code:'',
    seller_member:'',
    tag_id:''
};
        }
        function init2(){
 $scope.request2={
    site_domain:'',
    start_date:'',
    end_date:'',
    post_code:'',
    seller_member:'',
    tag_id:''
};
        }
        init1();
        init2();
        $scope.showAnotherDiv=false;

$scope.site_domain_list=['yahoo.com','mail.yahoo.com','msn.com','mail.aol.com','accuweather.com','ebay.com','people.com','outlook.live.com','apps.facebook.com','weather.com','dailymail.co.uk','finance.yahoo.com','lifedaily.com','cheatsheet.com','topix.net','twentytwowords.com','play.google.com',

'itunes.apple.com'];

$scope.reset1=function(){
    init1();
    
};
$scope.reset2=function(){
    init2();
};
$scope.showDiv=function(){
   $scope.showAnotherDiv=!$scope.showAnotherDiv;
};
function checkRequest1(){
    
    if(!$scope.request1.end_date&&!$scope.request1.start_date&&!$scope.request1.seller_member,
          !$scope.request1.post_code&&!$scope.request1.site_domain&&!$scope.request1.tag_id )
              return true;
          return false;
}
function checkRequest2(){
     if(!$scope.request2.end_date&&!$scope.request2.start_date&&!$scope.request2.seller_member,
          !$scope.request2.post_code&&!$scope.request2.site_domain&&!$scope.request2.tag_id )
              return true;
          return false;
}

$scope.generate=function(){
    var arr=[];
     
    console.log("request"+JSON.stringify($scope.request1));
    console.log("request"+JSON.stringify($scope.request2));
    if(checkRequest1()==false){
        arr.push($scope.request1);
    }
     if(checkRequest2()==false){
        arr.push($scope.request2);
    }
    console.log("arr"+JSON.stringify(arr));
    return $http.post('http://192.168.5.249:5000/generate-sketch',arr).then(function(response){
        console.log("response"+JSON.stringify(response.data));
        return response.data;
    });
}
}]);