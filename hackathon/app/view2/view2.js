'use strict';

angular.module('myApp.view2', ['ngRoute','chart.js'])

.config(['$routeProvider', function($routeProvider) {
  $routeProvider.when('/view2', {
    templateUrl: 'view2/view2.html',
    controller: 'View2Ctrl'
  });
}])

.controller('View2Ctrl', function ($scope, $http) {
  $scope.data;
  var fetchChartData = function() {
    return $http.post("http://192.168.5.249:5000/generate-sketch", {"Date": "test"})
    .then(chartData => {
      $scope.labels = chartData.data.xAxis;
      $scope.data =chartData.data.yAxis;
    });
  }

  fetchChartData();

});