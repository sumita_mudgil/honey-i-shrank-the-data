
import datasketch as ds
import sys
from pyspark.sql.types import StructType
from pyspark.sql.types import StructField
from pyspark.sql.types import IntegerType
from pyspark.sql.types import StringType
from pyspark.sql.functions import *

from pyspark import SparkContext
from pyspark.sql import SQLContext
from pyspark.sql import HiveContext

sc = SparkContext()
sqlContext = HiveContext(sc)
sql = sqlContext.sql

# load data
INPUT_BASE = 's3://dwh-reports-data/Bitanshu/hackIQ/dpi_feed/datacenter={}/supply_type=web/year={}/month={}/day={}/'

dc=sys.argv[1]
year=sys.argv[2]
month=sys.argv[3]
day=sys.argv[4]

input_url = INPUT_BASE.format(dc,year,month,day)
#input_url = '/data/'
dpi_feed = StructType([
StructField('UserId', IntegerType(), True),
StructField('sellerMemberId', StringType(), True),
StructField('siteDomain', StringType(), True),
StructField('degeopostcode', StringType(), True),
StructField('tagId', IntegerType(), True)
])
df = sqlContext.createDataFrame(sc.emptyRDD(), dpi_feed)
df = sqlContext.read.csv(input_url, header = False, sep = "\t")

df = df.select(col('_c0').alias('UserId'), col('_c1').alias('sellerMemberId'),
              col('_c2').alias('siteDomain'),
              col('_c3').alias('degeopostcode'),
              col('_c4').alias('tagId'))
df = df.repartition(50000)

def merge_minh(mh1 , mh2):
    mh1.merge(mh2)
    return mh1

def mih_init( seed = 2342, num_perm=128, field = 'siteDomain'):
    def _minh(rows):
        mh = ds.MinHash(seed = seed, num_perm = num_perm)
        for r in rows:
            mh.update(r['UserId'].decode('utf-8'))
        yield ( r[field], mh)
    return _minh

#gen_mh = mih_init(field='siteDomain')
#mh_rdd = df.rdd.map(gen_mh)
#mh_sd = mh_rdd.reduceByKey(merge_minh)
#mh_rdd.count()

def hllp_init( p = 8, field = 'siteDomain'):
    def _hpp(r):
        hp = ds.HyperLogLogPlusPlus(p = p)
        #sd = rows[0][field]
        #for r in rows:
        hp.update(r['UserId'])
        return (r[field], hp)
    return _hpp


gen_sd_hllp1 = hllp_init(field='siteDomain')
hllp_sd_rdd1 = df.rdd.map(gen_sd_hllp1)
final1 = hllp_sd_rdd1.reduceByKey(merge_minh)
x = final1.collect()
cPickle.dump(x,open('/mnt/cpkl/siteDomain.pkl','wb'))

os.system('aws s3 cp /mnt/cpkl/siteDomain.pkl s3://analyst-adhoc/adityaj/hackiq/result/dc={}/year={}/month={}/day={}/siteDomain/'\
            .format(dc,year,month,day))
del(x)

gen_sd_hllp = hllp_init(field='tagId')
hllp_sd_rdd = df.rdd.map(gen_sd_hllp)
final = hllp_sd_rdd.reduceByKey(merge_minh)
x = final.collect()
cPickle.dump(x,open('/mnt/cpkl/tag_id.pkl','wb'))

os.system('aws s3 cp /mnt/cpkl/tag_id.pkl s3://analyst-adhoc/adityaj/hackiq/result/dc={}/year={}/month={}/day={}/tagId/'\
            .format(dc,year,month,day))
del(x)

gen_sd_hllp2 = hllp_init(field='sellerMemberId')
hllp_sd_rdd2 = df.rdd.map(gen_sd_hllp2)
final2 = hllp_sd_rdd2.reduceByKey(merge_minh)
x = final2.collect()
cPickle.dump(x,open('/mnt/cpkl/sellerMemberId.pkl','wb'))
os.system('aws s3 cp /mnt/cpkl/sellerMemberId.pkl s3://analyst-adhoc/adityaj/hackiq/result/dc={}/year={}/month={}/day={}/sellerMemberId/'\
            .format(dc,year,month,day))
del(x)

gen_sd_hllp3 = hllp_init(field='degeopostcode')
hllp_sd_rdd3 = df.rdd.map(gen_sd_hllp3)
final3 = hllp_sd_rdd3.reduceByKey(merge_minh)
x = final3.collect()
cPickle.dump(x,open('/mnt/cpkl/degeopostcode.pkl','wb'))
os.system('aws s3 cp /mnt/cpkl/degeopostcode.pkl s3://analyst-adhoc/adityaj/hackiq/result/dc={}/year={}/month={}/day={}/degeopostcode/'\
            .format(dc,year,month,day))

