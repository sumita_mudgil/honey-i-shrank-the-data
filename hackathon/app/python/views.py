import cPickle as pickle

import redis
from flask import Flask, jsonify
from flask_caching import Cache
from flask_cors import CORS

from customer import Customer

app = Flask(__name__)
app.version = '0.3'
CORS(app)
r = redis.StrictRedis(host='localhost', port=6379, db=0)

cache = Cache(app, config={'CACHE_TYPE': 'simple'})


@app.route('/generate-sketch', methods=['GET', 'POST'])
def base():
    testRedisPoc()
    return jsonify({'xAxis': ['2006', '2007', '2008', '2009', '2010', '2011', '2012', '2006', '2007', '2008', '2009',
                              '2010', '2011', '2012'],
                    'yAxis': [65, 59, 80, 81, 56, 55, 40, 65, 59, 80, 81, 56, 55, 40]
                    })

def testRedisPoc():
    cust = Customer('Ford', 4)
    serCust = serializePythonObject(cust)
    set("test", serCust)
    x = deserializePythonObject(get("test"))
    print x.name

def get(key):
    return r.get(key)


def set(key, value):
    r.set(key, value)


def serializePythonObject(obj):
    return pickle.dumps(obj)

def deserializePythonObject(serObj):
    return pickle.loads(serObj)
