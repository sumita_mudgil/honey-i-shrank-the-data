
from itertools import combinations
import datasketch as ds
import redis
import cPickle
from datetime import datetime

r = redis.StrictRedis(host='localhost', port=6379, db=0)

def get(key):
    return r.get(key)

def put(key, value):
    r.set(key, value)


def serializePythonObject(obj):
    return cPickle.dumps(obj)

def deserializePythonObject(serobj):
    return cPickle.loads(serobj)

def make_key(f, v, d):
    dt = datetime.strptime(d[:10],'%Y-%m-%d')
    k = "{dc}:{year}:{month}:{day}:{field}:{value}"
    r = k.format( 
            dc = 'ams',
            year = dt.year,
            month = dt.month,
            day = dt.day,
            field = f ,
            value = v.encode('utf-8')
            )
    return r

def get_intersections(d,pkl_lookup):
    r = []
    for f in  ['siteDomain','sellerMemberId','degeopostcode','tagId']:
        hll = ds.HyperLogLogPlusPlus()
        if f == 'siteDomain':
            if len(d[f]) == 0:
                continue
            v = d[f]
        else:
            if len(d[f].strip()) == 0:
                continue
            v = [ x.strip() for x in d[f].split(',') ]
        for each in v:
            sketch = pkl_lookup[ make_key(f, each, d['start_date'])]
            print  '--'
            print sketch
            print '--'
            hll.merge(sketch)
        r.append(hll)
    return r

def fetch_objs(keys):
    obj_dict = {}
    for k in keys:
        try:
            o = get(k)
            o = deserializePythonObject(o)
            obj_dict[k] =o 
        except Exception as e:
            print e
    return obj_dict

def perform_intersection( sets ):
     
    hll = ds.HyperLogLogPlusPlus()
    # l1
    for s in sets:
        hll.merge(s)
    
    combinations = []
    for n in range(2,len(sets)):
        for c in combinations(sets, n):
            h = ds.HyperLogLogPlus()
            for e in c:
                h.merge(e)
            
            combinations.append(h)
    
    substract = sum([ x.count() for x in combinations ])
    union_all = hll.count()
    return union_all - substract

def process_input(j):
    fixed_input = []
    for s in j:
        t = { 'siteDomain' : s['site_domain'],
                'sellerMemberId' : s['seller_member'],
                'tagId' : s['tag_id'],
                'degeopostcode' : s['post_code'],
                'start_date': s['start_date']
                }
        fixed_input.append(t)
    
    j = fixed_input
    key_list = []
    for s in j:
        for f in  ['siteDomain','sellerMemberId','degeopostcode','tagId']:
            if f == 'siteDomain':
                v = s[f]
            else:
                if len(s[f].strip()) == 0:
                    continue
                v = [ x.strip() for x in s[f].split(',') ]
            
            key_list.extend( [ make_key( f, v1 , s['start_date']) for v1 in v ]  ) 
    
    pkl_lookup = fetch_objs(key_list)
    # load all objs
    #for setup in j:
    inters = []
    for s in j:
        inters.extend(get_intersections(s,pkl_lookup))
    
    r = perform_intersection(inters)
    return r
        
